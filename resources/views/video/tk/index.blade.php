<html>
<head>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
</head>
<body>
<div class="container mt-3">
<div class="drag-area">
    <form method="post" action="{{ Route('insert.filetk') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
        <label>Tambakan Url Youtube Video</label>
        <input class="form-control" id="formFileLg" type="text" name="video" placeholder="https://www.youtube.com/embed/code-youtube"/>
    </div>
        <p>
         @if($errors->has('video'))
           {{ $errors->first('video') }}
         @endif
        </p>
    <div class="form-group mt-3">
      <label>Judul</label>
      <input class="form-control" name="title" placeholder="Masukkan Judul Video"/>
    </div>
    <div class="form-group my-3">
        <label for="exampleFormControlTextarea1" class="form-label">Deskripsi</label>
        <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3" placeholder="Masukkan Deskripsi"></textarea>
    </div>
    <input type="reset" name="click" class="btn btn-danger"/>
    <input type="submit" name="click" class="btn btn-primary"/>
    </form>
</div>
</div>
</body>
</html>