<html>

<head>
  <title>Belajar Motion Graphic</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
  <link href="https://vjs.zencdn.net/7.18.1/video-js.css" rel="stylesheet" />
</head>

<body>
  @include('video.tk.nav')
  <main class="container fluid">
    <div class="row mt-3 gx-3 gy-3">
      @foreach($data as $row)
      <div class="card p-0 col-12" style="font-family: Century Gothic;">
        <!-- <div class="card-header">
          <h1>{{$row->title}}</h1>
        </div> -->
        <div class="card-body row">
          <div class="col-md-4">
            <h1>{{$row->title}}</h1>
            <p class="card-text">{{$row->description}}</p>
          </div>
          <div class="col-md-8">
            <iframe class="w-100 rounded" height="400" src="{{$row->video}}" title="{{$row->title}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </main>

  <script src="https://vjs.zencdn.net/7.18.1/video.min.js"></script>
</body>

</html>