@extends('template')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Data Post Materi</div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="">Category Id</label>
                            <input type="text" name="" class="form-control" value="{{ $data->category_id }}"
                                readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Title</label>
                            <input type="text" name="title" value="{{ $data->title }}" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Deskripsi</label>
                            <input type="text" name="excerpt" value="{{ $data->excerpt }}" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Link</label>
                            <input type="text" name="body" value="{{ $data->body }}" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <a href="{{ url('post') }}" class="btn btn-block btn-outline-primary">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection