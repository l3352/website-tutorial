@extends('template')
@include('layouts.sidebar')

<div class="container">
    <div class="row">
      <div class="col">
        <div class="card">
          <div class="card-header">
            Post Materi
              <a href="{{ route('post.create') }}" class="btn btn-sm btn-info" style="float: right">
                add Data
              </a>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <th>Category</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Link</th>
                </tr>
                @foreach ($data as $data)
                <tr>
                  <td>{{ $data->category->name }}</td>
                  <td>{{ $data->title }}</td>
                  <td>{{ $data->excerpt }}</td>
                  <td>{{ $data->body }}</td>
                  <td>
                    <form action="{{ route('post.destroy', $data->id) }}" method="post">
                      @csrf
                      @method('DELETE')
                      <a href="{{ route('post.edit', $data->id) }}" class="btn btn-outline-success">Edit</a>
                      <a href="{{ route('post.show', $data->id) }}" class="btn btn-outline-warning">Show</a>
                      <button type="submit" class="btn btn-outline-danger" onclick="return confirm('are you sure?')">Delete</button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<link rel="stylesheet" href="{{asset('template/css/styles3.css')}}">
<script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous"></script>
