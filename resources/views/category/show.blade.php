@extends('template')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">Category</div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="">ID</label>
                            <input type="text" name="id" class="form-control" value="{{ $category->id }}"
                                readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control" value="{{ $category->name }}" readonly>
                        </div>
                        <div class="form-group">
                            <br>
                            <a href="{{ route('category.index') }}" class="btn btn-block btn-outline-primary">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection