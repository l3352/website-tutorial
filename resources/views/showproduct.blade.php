<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<center><h1>Data Video Pembelajaran</h1></center>


<button type="button" class="btn btn-success">Create</button>
<div class="card-body table-full-width table-responsive">
<table class="table table-bordered">
  <thead class= "table-dark">
    <tr>
        <th>Link</th>
        <th>Title</th>
        <th>Category</th>
        <th>Description</th>
        <th>File</th>
        <th>Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $data)
    <tr>
        <td>{{$data->link}}</td>
        <td>{{$data->judul}}</td>
        <td>{{$data->kategori}}</td>
        <td>{{$data->deskripsi}}</td>
        <td>{{$data->file}}</td>
        <td>
            <button type="button" class="btn btn-primary">Update</button>
            <button type="button" class="btn btn-danger">Delete</button>
        </td>
    @endforeach   
    </tr>
  </tbody>
</table>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>