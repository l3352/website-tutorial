<footer class="footer text-center">
            <div class="container">
                <div class="row">
                    <!-- Footer Location-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Location</h4>
                        <p class="lead mb-0">
                            Jl. Pesantren No.KM.2, Cibabat, Kec. Cimahi Utara 
                            <br />
                            Kota Cimahi, Jawa Barat 40513
                        </p>
                    </div>
                    <!-- Footer Social Icons-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">SOCIAL MEDIA</h4>
                        <a class="btn btn-outline-light btn-social mx-1" href="https://www.instagram.com/loot.fee/"><i class="fab fa-fw fa-instagram"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="https://wa.me/6289662186564"><i class="fab fa-fw fa-whatsapp"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="https://www.youtube.com/channel/UChVWNBF6G_2glPHuwqQbd6w"><i class="fab fa-fw fa-youtube"></i></a>
                    </div>
                    <!-- Footer About Text-->
                    <div class="col-lg-4">
                        <h4 class="text-uppercase mb-4">About</h4>
                        <p class="lead mb-0">
                        Di sini kalian akan bisa mendapatkan hal yang baru dan tentunya bisa memilik keahlian baru , untuk membantu kalian menghdapi persaingan di dunia kerja

                        </p>
                    </div>
                </div>
            </div>
</footer>