<link rel="stylesheet" href="{{asset('template/css/styles3.css')}}">
<script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
<div class="wrapper">
    <div class="sidebar">
        <h2><a href="http://localhost:8000/admin/home" style="text-decoration : none; color: #fff;">Admin</a></h2>
        <ul>
            <li><a href="http://localhost:8000/user"><i class="fas fa-users"></i>User</a></li>
            <li><a href="http://localhost:8000/post"><i class="fas fa-folder"></i>Content</a></li>
            <li><a href="http://localhost:8000/category"><i class="fas fa-list-alt"></i>Category</a></li>
        </ul>
    </div>
    <div class="main_content">
        <div class="header"><form method="post" action="{{ route('logout') }}">
                        @csrf
                        <button class="btn btn-outline-warning" type="submit" style="margin-right: -90px;">Logout</form></button>
                </div>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous"></script> 
