<?php

use App\Models\Post;
use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PostMateriController;
use App\Http\Controllers\CategoryController;


Route::get('/', function () {
    return view('welcome');
});
// Route Crud User
Route::resource('user', UserController::class);

//Route post
Route::get('/posts',[App\Http\Controllers\PostController::class,'index']);
Route::get('/posts/{id}', [App\Http\Controllers\PostController::class,'show']);

//Route Crud Materi
Route::resource('post', PostMateriController::class);
Route::resource('category', CategoryController::class);

Route::get('/categories/{category::slug}', function(Category $category){
    return view('category', [
        'title' => $category->name,
        'posts' => $category->posts,
        'category' => $category->name
    ]);
});


Auth::routes();

//Route Halaman Admin dan User
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('admin/home',[App\Http\Controllers\HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');

