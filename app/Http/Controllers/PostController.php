<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;

class PostController extends Controller
{
    public function index(Request $request)
    {
        $withFilter = null;
        $data = Post::all();
        if($request->category_id){
          $data = Post::where('category_id', $request->category_id)->get();
          $withFilter = Category::find($request->category_id);
        }
        // dd($withFilter);
        return view('posts', [
            "title" => "Posts",
            "posts" => $data,
            'withFilter' => $withFilter
        ]);
    }

    public function show($id)
    {
        return view('post', [
            "title" => "Single Post",
            "post" => Post::find($id)
        ]);
    }
}
