<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\web;

class WebController extends Controller
{
    //
        function index()
    {
        return view("video.web.index");
    }

    public function admin()
    {
        $data = web::all();
        return view('video.web.admin',compact('data'));
    }



    function fetch()
    {
        $data = web::all();
        return view('video.web.videos', compact('data'));
    }

    function insert(Request $request)
    {
        $request->validate([
            'video' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        web::create($request->all());

        return redirect('video/crudweb');
    }
    public function edit($id)
    {
        $data = web::find($id);
        return view('video.web.edit',compact('data'));
    }

    public function updatedata(Request $request, $id)
    {
        $data = web::find($id);
        $data->update($request->all());

        return redirect()->route('video.web.admin')->with('success','Data Berhasil Di Update');
    }

    public function delete($id)
    {
        $data = web::find($id);
        $data->delete();

        return redirect()->route('video.web.admin')->with('success','Data Berhasil Di Delete');
    }
}
