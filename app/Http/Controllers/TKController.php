<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tk;

class TKController extends Controller
{
    function index()
    {
        return view("video.tk.index");
    }

    public function admin()
    {
        $data = tk::all();
        return view('video.tk.admin',compact('data'));
    }



    function fetch()
    {
        $data = tk::all();
        return view('video.tk.videos', compact('data'));
    }

    function insert(Request $request)
    {
        $request->validate([
            'video' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        tk::create($request->all());

        return redirect('video/crudtk');
    }
    public function edit($id)
    {
        $data = tk::find($id);
        return view('video.tk.edit',compact('data'));
    }

    public function updatedata(Request $request, $id)
    {
        $data = tk::find($id);
        $data->update($request->all());

        return redirect()->route('video.tk.admin')->with('success','Data Berhasil Di Update');
    }

    public function delete($id)
    {
        $data = tk::find($id);
        $data->delete();

        return redirect()->route('video.tk.admin')->with('success','Data Berhasil Di Delete');
    }
}
