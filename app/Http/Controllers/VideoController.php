<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;

class VideoController extends Controller
{
    public function uploadpage()
    {
        return view('product');
    }
    public function store(Request $request)
    {
        $data=new product();

        $file=$request->file;
        $filename=time().'.'.$file->getClientOriginalExtension();
        $request->file->move('assets',$filename);
        $data->file=$filename;

         
        $data->title=$request->title;
        $data->judul=$request->judul;
        $data->kategori=$request->kategori;
        $data->deskripsi=$request->deskripsi;

        $data->save();
        return view('showproduct');
    }
    public function show()
    {
        $data=product::all();
        return view('showproduct',compact('data'));
    }
    public function view($id)
    {
        $data=Product::find($id);

        return view('viewproduct',compact('data'));
    }
}
