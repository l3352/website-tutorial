<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Product;

class PageController extends Controller
{
    public function uploadpage()
    {
        return view('createproduct');
    }
    public function store(Request $request)
    {
        $data=new product();

        $file=$request->file;
        $filename=time().'.'.$file->getClientOriginalExtension();
        $request->file->move('assets',$filename);
        $data->file=$filename;

        $data->link=$request->link;
        $data->judul=$request->judul;
        $data->kategori=$request->kategori;
        $data->deskripsi=$request->deskripsi;

        $data->save();
        return redirect()->back();
    }
    public function show()
    {
        $data=product::all();
        return view('showproduct',compact('data'));
    }
    public function view($kategori)
    {
        $data=Product::find($kategori);

        return view('viewproduct',compact('data'));
    }
}
