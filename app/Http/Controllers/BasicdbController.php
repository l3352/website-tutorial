<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\basicdb;

class BasicdbController extends Controller
{
    function index()
    {
        return view("video.basicdb.index");
    }

    public function admin()
    {
        $data = basicdb::all();
        return view('video.basicdb.admin',compact('data'));
    }



    function fetch()
    {
        $data = basicdb::all();
        return view('video.basicdb.videos', compact('data'));
    }

    function insert(Request $request)
    {
        $request->validate([
            'video' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        basicdb::create($request->all());

        return redirect('video/crud-db');
    }
    public function edit($id)
    {
        $data = basicdb::find($id);
        return view('video.basicdb.edit',compact('data'));
    }

    public function updatedata(Request $request, $id)
    {
        $data = basicdb::find($id);
        $data->update($request->all());

        return redirect()->route('video.basicdb.admin')->with('success','Data Berhasil Di Update');
    }

    public function delete($id)
    {
        $data = basicdb::find($id);
        $data->delete();

        return redirect()->route('video.basicdb.admin')->with('success','Data Berhasil Di Delete');
    }
}
