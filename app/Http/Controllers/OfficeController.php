<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OfficeController extends Controller
{
        function index()
    {
        return view("video.office.videos");
    }

    public function admin()
    {
        $data = office::all();
        return view('video.office.admin',compact('data'));
    }



    function fetch()
    {
        $data = office::all();
        return view('video.office.videos', compact('data'));
    }

    function insert(Request $request)
    {
        $request->validate([
            'video' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        office::create($request->all());

        return redirect('video/crudoffice');
    }
    public function edit($id)
    {
        $data = office::find($id);
        return view('video.office.edit',compact('data'));
    }

    public function updatedata(Request $request, $id)
    {
        $data = web::find($id);
        $data->update($request->all());

        return redirect()->route('video.office.admin')->with('success','Data Berhasil Di Update');
    }

    public function delete($id)
    {
        $data = web::find($id);
        $data->delete();

        return redirect()->route('video.office.admin')->with('success','Data Berhasil Di Delete');
    }
}
