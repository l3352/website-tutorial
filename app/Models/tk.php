<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tk extends Model
{
    protected $table = "tks";
    protected $fillable = ['video','title','description'];
}
