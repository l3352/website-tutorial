<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class basicdb extends Model
{
    protected $table = "basicdbs";
    protected $fillable = ['video','title','description'];
}
